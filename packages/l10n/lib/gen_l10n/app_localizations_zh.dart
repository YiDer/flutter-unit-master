import 'app_localizations.dart';

/// The translations for Chinese (`zh`).
class AppLocalizationsZh extends AppLocalizations {
  AppLocalizationsZh([String locale = 'zh']) : super(locale);

  @override
  String get widgetCollection => '组件集录';

  @override
  String get paintCollection => '绘制集录';

  @override
  String get knowledgeCollection => '知识集锦';

  @override
  String get treasureTools => '工具宝箱';
}
